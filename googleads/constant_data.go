package v201809

import (
	"encoding/xml"
)

type ConstantDataService struct {
	Auth
}

func NewConstantDataService(auth *Auth) *ConstantDataService {
	return &ConstantDataService{Auth: *auth}
}

func (s *ConstantDataService) GetAgeRangeCriterion(customerID string) (ageRanges []AgeRangeCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getAgeRangeCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getAgeRangeCriterion"`
		}{},
	)
	if err != nil {
		return ageRanges, err
	}
	getResp := struct {
		AgeRangeCriterions []AgeRangeCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return ageRanges, err
	}
	return getResp.AgeRangeCriterions, err
}

func (s *ConstantDataService) GetCarrierCriterion(customerID string) (carriers []CarrierCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getCarrierCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getCarrierCriterion"`
		}{},
	)
	if err != nil {
		return carriers, err
	}
	getResp := struct {
		CarrierCriterions []CarrierCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return carriers, err
	}
	return getResp.CarrierCriterions, err
}

func (s *ConstantDataService) GetGenderCriterion(customerID string) (genders []GenderCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getGenderCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getGenderCriterion"`
		}{},
	)
	if err != nil {
		return genders, err
	}
	getResp := struct {
		GenderCriterions []GenderCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return genders, err
	}
	return getResp.GenderCriterions, err
}

func (s *ConstantDataService) GetLanguageCriterion(customerID string) (languages []LanguageCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getLanguageCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getLanguageCriterion"`
		}{},
	)
	if err != nil {
		return languages, err
	}
	getResp := struct {
		LanguageCriterions []LanguageCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return languages, err
	}
	return getResp.LanguageCriterions, err
}

func (s *ConstantDataService) GetMobileDeviceCriterion(customerID string) (mobileDevices []MobileDeviceCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getMobileDeviceCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getMobileDeviceCriterion"`
		}{},
	)
	if err != nil {
		return mobileDevices, err
	}
	getResp := struct {
		MobileDeviceCriterions []MobileDeviceCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return mobileDevices, err
	}
	return getResp.MobileDeviceCriterions, err
}

func (s *ConstantDataService) GetOperatingSystemVersionCriterion(customerID string) (operatingSystemVersions []OperatingSystemVersionCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getOperatingSystemVersionCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getOperatingSystemVersionCriterion"`
		}{},
	)
	if err != nil {
		return operatingSystemVersions, err
	}
	getResp := struct {
		OperatingSystemVersionCriterions []OperatingSystemVersionCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return operatingSystemVersions, err
	}
	return getResp.OperatingSystemVersionCriterions, err
}

func (s *ConstantDataService) GetProductBiddingCategoryCriterion(customerID string, selector Selector) (categoryData []ProductBiddingCategoryData, err error) {
	selector.XMLName = xml.Name{baseUrl, "selector"}

	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getProductBiddingCategoryData",
		struct {
			XMLName xml.Name
			Sel     Selector
		}{
			XMLName: xml.Name{
				Space: "https://adwords.google.com/api/adwords/cm/v201809",
				Local: "getProductBiddingCategoryData",
			},
			Sel: selector,
		},
	)
	if err != nil {
		return categoryData, err
	}
	getResp := struct {
		ProductBiddingCategoryDatas []ProductBiddingCategoryData `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return categoryData, err
	}
	return getResp.ProductBiddingCategoryDatas, err
}

func (s *ConstantDataService) GetUserInterestCriterion(customerID string) (userInterests []UserInterestCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getUserInterestCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getUserInterestCriterion"`
		}{},
	)
	if err != nil {
		return userInterests, err
	}
	getResp := struct {
		UserInterestCriterions []UserInterestCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return userInterests, err
	}
	return getResp.UserInterestCriterions, err
}

func (s *ConstantDataService) GetVerticalCriterion(customerID string) (verticals []VerticalCriterion, err error) {
	respBody, err := s.Auth.request(
		customerID,
		constantDataServiceUrl,
		"getVerticalCriterion",
		struct {
			XMLName xml.Name `xml:"https://adwords.google.com/api/adwords/cm/v201809 getVerticalCriterion"`
		}{},
	)
	if err != nil {
		return verticals, err
	}
	getResp := struct {
		VerticalCriterions []VerticalCriterion `xml:"rval"`
	}{}
	err = xml.Unmarshal([]byte(respBody), &getResp)
	if err != nil {
		return verticals, err
	}
	return getResp.VerticalCriterions, err
}
